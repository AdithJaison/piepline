pipeline {
  agent { label "master" }
  stages {
    stage("Build APp") {
      steps {
        sh """
          cd app && docker build -t app .
        """
      }
    }
    stage("Build Nginx") {
      steps {
        sh """
          cd nginx && docker build -t nginx .
        """
      }
    }
    stage("Build postgres") {
      steps {
        sh """
          cd postgres && docker build -t postgres .
        """
      }
    }
    stage("Run APp") {
      steps {
        sh """
          docker-compose -f docker-compose.prod.yml up --build -d
        """
      }
    }
  }
}
